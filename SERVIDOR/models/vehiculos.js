'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class vehiculos extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  vehiculos.init({
    descripcion: DataTypes.STRING,
    marca: DataTypes.STRING,
    modelo: DataTypes.STRING,
    placa: DataTypes.STRING,
    clienteid: DataTypes.INTEGER,
    anio: DataTypes.INTEGER,
    mes_rtv: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'vehiculos',
  });

  // Obtener todos los vehiculos
  vehiculos.obtenerVehiculos = () => {
    return sequelize.query(
      `select v.id,concat(cl.nombre,' ',cl."primerApellido",' ',cl."segundoApellido") as cliente,
      v.marca, v.modelo,v.placa,v.anio
      from vehiculos v
      inner join clientes cl ON cl.id = v.clienteid order by v.marca, v.modelo`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

  // Obtener vehiculo por id
  vehiculos.obtenerVehiculosPorId = (id) => {
    return sequelize.query(
      `select  cl.id as clienteid,cl.cedula,v.marca, v.modelo,v.placa,v.anio
      from vehiculos v
      inner join clientes cl ON cl.id = v.clienteid
      where v.id = ${id}`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

  return vehiculos;
};