'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class clientes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  clientes.init({
    nombre: DataTypes.STRING,
    primerApellido: DataTypes.STRING,
    segundoApellido: DataTypes.STRING,
    cedula: DataTypes.STRING,
    fechaNacimiento: DataTypes.DATE,
    correo: DataTypes.STRING,
    telefono: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'clientes',
  });
  
   // Obtener todos los clientes
   clientes.obtenerClientes = () => {
    return sequelize.query(
      `select id,concat(nombre,' ',"primerApellido",' ',"segundoApellido") as nombre,cedula
      ,to_char( "fechaNacimiento", 'MM-DD-YYYY') as fechanacimiento ,correo,telefono
      from clientes
      order by nombre asc`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

    // Obtener cliente por Id
    clientes.obtenerClienteId = (id) => {
      return sequelize.query(
        `select id,concat(nombre,' ',"primerApellido",' ',"segundoApellido") as nombre,cedula
        ,to_char( "fechaNacimiento", 'MM-DD-YYYY') as fechanacimiento ,correo,telefono
        from clientes
        where id = ${id}`,
        { type: sequelize.QueryTypes.SELECT }
      );
    };

  return clientes;
};