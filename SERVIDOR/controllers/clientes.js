let models = require("../models");
const sequelize = require('sequelize');

/**Crear cliente */
exports.crear = async (req, res) => {
  let data = req.body;
  try {
    await models.clientes.create(data);
    res.status(201).json("Cliente creado con éxito.")
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Obtener todos los clientes */
exports.obtener = async (req, res) => {
  try {
    const clientes = await models.clientes.obtenerClientes();
    res.status(200).json(clientes)
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Obtener clientes Dropdown */
exports.obtenerDropdown = async (req, res) => {
  try {
    const clientes = await models.clientes.findAll({attributes: [["id","value"],["cedula","label"]],order: [['cedula', 'ASC']],raw: true});
    res.status(200).json(clientes)
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Obtener cliente en específico por ID */
exports.obtenerCliente = async (req, res) => {
  const {id} = req.params;
  try {
    const cliente = await models.clientes.findOne({attributes: ["nombre","primerApellido","segundoApellido","cedula","fechaNacimiento","correo","telefono"]
    ,where:{id},raw: true});
    res.status(200).json(cliente)
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Actualizar cliente */
exports.actualizar = async (req, res) => {
  const {id} = req.params;
  let data = req.body; 
  try {
    await models.clientes.update(data, {where: { id }});    
    res.status(200).json("Cliente actualizado con éxito.");

  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Eliminar cliente */
exports.eliminar = async (req, res) => {
  const {id} = req.params;
  try {
    await models.clientes.destroy({where: { id }});    
    res.status(200).json("Cliente eliminado con éxito.");

  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};
