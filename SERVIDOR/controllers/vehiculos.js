let models = require("../models");
const sequelize = require('sequelize');

/**Crear vehiculo */
exports.crear = async (req, res) => {
  let data = req.body;
  try {
    await models.vehiculos.create(data);
    res.status(201).json("Vehiculo creado con éxito.")
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Obtener todos los vehiculos */
exports.obtener = async (req, res) => {
  try {
    const vehiculos = await models.vehiculos.obtenerVehiculos();
    res.status(200).json(vehiculos)
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Obtener vehiculo en específico por ID */
exports.obtenerVehiculo = async (req, res) => {
  const {id} = req.params;
  try {
    const vehiculo = await models.vehiculos.obtenerVehiculosPorId(id) ;
    res.status(200).json(vehiculo)
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Actualizar vehiculo */
exports.actualizar = async (req, res) => {
  const {id} = req.params;
  let data = req.body; 
  try {
    await models.vehiculos.update(data, {where: { id }});    
    res.status(200).json("Vehículo actualizado con éxito.");

  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Eliminar vehiculo */
exports.eliminar = async (req, res) => {
  const {id} = req.params;
  try {
    await models.vehiculos.destroy({where: { id }});    
    res.status(200).json("Vehículo eliminado con éxito.");

  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};
