# Instalar las dependencias

    npm install

# En config/config.json realizar las siguientes configuracion para conectar a su DB
    En development
      "username": "username",  --Su usuario de la DB
      "password": "password",  --Su contraseña
      "database": "Database",  --Nombre de la Base de datos
      "host": "127.0.0.1",     --Ip de la Base de Datos
      "port":5432,             --Puerto
      "dialect": "postgres",   --Gestor de la Base de Datos. Ejemplo:  mssql, si deseo usar para sqlServer
      "logging": false  

# Realizar migraciones

    sequelize db:migrate

# Iniciar el servidor

    node server.js