const models = require("../models");

const clienteExiste = async (cedula) => {
  const cliente = await models.clientes.findOne({
    attributes: ["cedula"],
    where: { cedula },
  });
  if (cliente) {
    throw new Error(`La cédula ${cedula}, ya ha sido registrada anteriormente`);
  }
};

const vehiculoExiste = async (placa) => {
  const vehiculo = await models.vehiculos.findOne({
    attributes: ["placa"],
    where: { placa },
  });
  if (vehiculo) {
    throw new Error(`La placa ${placa}, ya ha sido registrada anteriormente`);
  }
};

const vehiculoActualizarExiste = async (vehiculo) => {
  console.log(vehiculo)
  // const vehiculo = await models.vehiculos.findOne({
  //   attributes: ["placa"],
  //   where: { placa },
  // });
  // if (vehiculo) {
  //   throw new Error(`La placa ${placa}, ya ha sido registrada anteriormente`);
  // }
};



module.exports = {
  clienteExiste,
  vehiculoExiste,
  vehiculoActualizarExiste
};
