const { clienteExiste } = require("../helper/dbValidator");

module.exports = (app) => {
  const clientes = require("../controllers/clientes");
  const { check } = require('express-validator');
  const {validarbody} = require('../midleware');
  const router = require("express").Router();

  /**Crear Cliente */
  router.post("/",[
  check('nombre').isLength({min:5, max:100}).withMessage('Debe tener de 5 a 100 carácteres'),
  check('primerApellido').isLength({min:5, max:100}).withMessage('Debe tener de 5 a 100 carácteres'),
  check('segundoApellido').isLength({min:5, max:100}).withMessage('Debe tener de 5 a 100 carácteres'),
  check('cedula').isLength({min:9, max:30}).withMessage('Debe tener de 9 a 30 carácteres'),
  check('cedula').custom( clienteExiste ),
  check('fechaNacimiento').isISO8601('yyyy-mm-dd').withMessage('Debe ser una fecha válida'),
  check('telefono').isNumeric().withMessage('Digite solo números').isLength({min:8, max:8}).withMessage('Debe tener 8 carácteres'),
  check('correo').isEmail().withMessage('Debe ser un correo válido'),
  
  validarbody
  ], clientes.crear);

  /**Obtener Clientes*/
  router.get("/",clientes.obtener);

   /**Obtener Clientes*/
   router.get("/dropdown",clientes.obtenerDropdown);

  /**Obtener Cliente por id*/
  router.get("/:id",clientes.obtenerCliente);

  /**Actualizar Cliente*/
  router.put("/:id", 
  [
    check('nombre').isLength({min:5, max:100}).withMessage('Debe tener de 5 a 100 carácteres'),
    check('primerApellido').isLength({min:5, max:100}).withMessage('Debe tener de 5 a 100 carácteres'),
    check('segundoApellido').isLength({min:5, max:100}).withMessage('Debe tener de 5 a 100 carácteres'),
    check('cedula').isLength({min:9, max:30}).withMessage('Debe tener de 9 a 30 carácteres'),
    // check('cedula').custom( clienteExiste ),
    check('fechaNacimiento').isISO8601('yyyy-mm-dd').withMessage('Debe ser una fecha válida'),
    check('telefono').isNumeric().withMessage('Digite solo números').isLength({min:8, max:8}).withMessage('Debe tener 8 carácteres'),
    check('correo').isEmail().withMessage('Debe ser un correo válido'),
    validarbody
  ],clientes.actualizar);

  /**Eliminar Cliente */
  router.delete("/:id", clientes.eliminar);
 
  app.use("/api/clientes", router);
};
