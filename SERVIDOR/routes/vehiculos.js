 const { vehiculoExiste } = require("../helper/dbValidator");

module.exports = (app) => {
  const vehiculos = require("../controllers/vehiculos");
  const { check } = require('express-validator');
  const {validarbody} = require('../midleware');
  const router = require("express").Router();

  /**Crear Vehiculo */
  router.post("/",[
  check('marca').isLength({min:5, max:50}).withMessage('Debe tener de 5 a 50 carácteres'),
  // check('modelo').isLength({min:5, max:100}).withMessage('Debe tener de 5 a 50 carácteres'),
  check('placa').isLength({min:5, max:7}).withMessage('Debe tener de 1 a 7 carácteres'),
  check('placa').custom( vehiculoExiste ),
  check('clienteid').not().isEmpty(),
  check('anio').isNumeric().withMessage('Digite solo números').isLength({min:4, max:4}).withMessage('Debe tener 4 carácteres'),
  validarbody
  ], vehiculos.crear);

  /**Obtener vehiculos*/
  router.get("/",vehiculos.obtener);

  //  /**Obtener vehiculos*/
  //  router.get("/dropdown",vehiculos.obtenerDropdown);

  /**Obtener Cliente por id*/
  router.get("/:id",vehiculos.obtenerVehiculo);

  /**Actualizar Cliente*/
  router.put("/:id", 
  [
    check('marca').isLength({min:5, max:50}).withMessage('Debe tener de 5 a 50 carácteres'),
    check('placa').isLength({min:5, max:7}).withMessage('Debe tener de 1 a 7 carácteres'),
    check('clienteid').not().isEmpty(),
    check('anio').isNumeric().withMessage('Digite solo números').isLength({min:4, max:4}).withMessage('Debe tener 4 carácteres'),
    validarbody
  ],vehiculos.actualizar);

  /**Eliminar Cliente */
  router.delete("/:id", vehiculos.eliminar);
 
  app.use("/api/vehiculos", router);
};
