import React from "react";
import Layout from "../../Components/Sidebar/Layout";

const Prueba = () => {
  let height = window.screen.height > 1030 ? window.screen.height / 2.9 : window.screen.height/3.8;
  console.log(window.screen.height)
  return (
    <Layout titulo="INICIO">
      <div className="div-principal" style={{height:`${height}px`, backgroundColor:'red'}}>
      </div>
      <div className="div-principal" style={{height:`${height}px`, backgroundColor:'yellow'}}>
      </div>
      <div className="div-principal" style={{height:`${height}px`, backgroundColor:'blue'}}>
      </div>
    </Layout>
  );
};

export default Prueba;
