import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getClienteDropdownAction } from "../../actions/clienteAction";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import { Input } from "../../Components/Inputs/Input";
import Layout from "../../Components/Sidebar/Layout";
import {
  erroresApiMostrar,
  validarFormulario,
} from "../../helper/validarFormulario";
import { ContenedorError } from "../../Components/Inputs/ContenedorError";
import SelectSearch from "../../Components/Select/SelectSearch";
import {
  addVehiculoAction,
  getVehiculoIdAction,
} from "../../actions/vehiculoAction";
// Iconos
import PersonIcon from '@mui/icons-material/Person';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import BookmarksIcon from '@mui/icons-material/Bookmarks';  
import Looks3Icon from '@mui/icons-material/Looks3';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';

const Formulario = () => {
  // Parámetro del id
  const { id } = useParams();

  // Historial del router
  const history = useHistory();

  // Dispara una acción en el action
  const dispatch = useDispatch();

  // estado para si es un formulario para editar o crear
  const [edit, setEdit] = useState(false);
  const [error, setError] = useState(false);

  // Validar
  const [errors, setErrors] = useState({
    marca: "",
    modelo: "",
    placa: "",
    anio: "",
    clienteid: "",
  });

  const [seleccionarCliente, setSeleccionarCliente] = useState('');

  const [vehiculo, setVehiculo] = useState({
    marca: "",
    modelo: "",
    placa: "",
    anio: "",
    clienteid: "",
  });

  const onChange = (e) => {
    setVehiculo({
      ...vehiculo,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    if(seleccionarCliente !==''){
      setVehiculo({
        ...vehiculo,
        clienteid: seleccionarCliente.value.toString(),
      });
    }
   
    // eslint-disable-next-line
  }, [seleccionarCliente]);

  // Estado que en el reducer pasa a true si fue enviado
  const sending = useSelector((state) => state.vehiculos.sending);
  const errorEnvio = useSelector((state) => state.vehiculos.error);
  const erroresEnvio = useSelector((state) => state.vehiculos.errores);
  const clientes = useSelector((state) => state.clientes.clientes);
  const vehiculoEdit = useSelector((state) => state.vehiculos.vehiculoEdit);

  useEffect(() => {
    if (id !== undefined) {
      setEdit(true);
      dispatch(getVehiculoIdAction(id));
    } else {
      setEdit(false);

      clearForm();
    }
    // eslint-disable-next-line
  }, [id]);

  useEffect(() => {
    if (vehiculoEdit !== null) {
      setSeleccionarCliente({label:vehiculoEdit.cedula, value:vehiculoEdit.clienteid})
      setVehiculo(vehiculoEdit);
    }
    // eslint-disable-next-line
  }, [vehiculoEdit]);

  // Obtener datos para cargar el dropdown
  useEffect(() => {
    dispatch(getClienteDropdownAction());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (sending) {
      clearForm();
      
      if (sending && edit) {
        console.log("actualizado")
        history.push(`/vehiculos`);
      }
    }

    // eslint-disable-next-line
  }, [sending]);

  useEffect(() => {
    if (errorEnvio) {
      setErrors(erroresApiMostrar(vehiculo, erroresEnvio).objErrores);
      setError(true);
    }
    // eslint-disable-next-line
  }, [errorEnvio]);

  const addVehiculo = async (e) => {
    e.preventDefault();
    await setError(false);
    setErrors(validarFormulario(vehiculo).objErrores);

    if (validarFormulario(vehiculo).validarError) {
      setError(true);
    } else {
      setError(false);
      dispatch(addVehiculoAction(vehiculo, edit, id));
    }
  };

  const clearForm = () => {
    setVehiculo({
      marca: "",
      modelo: "",
      placa: "",
      anio: "",
      clienteid: "",
    });
    setSeleccionarCliente('');
  };

  const { marca, modelo, placa, anio } = vehiculo;

  let height = window.screen.height - 300;

  return (
    <Layout titulo={edit ? "Editar Vehículo" : "Crear Vehículo"}>
      <>
        {/* Contenedor de Error */}
        <ContenedorError error={error} />

        <form className="row g-3 needs-validation" onSubmit={addVehiculo}>
          <SelectSearch
            setChange={setSeleccionarCliente}
            valor={seleccionarCliente}
            opciones={clientes}
            name="Cliente"
            error={errors.clienteid}
            icono={<PersonIcon fontSize="large" />}
          />
          <Input
            name="marca"
            onChange={onChange}
            value={marca}
            label="Marca"
            error={errors.marca}
            placeholder="Marca"
            icono={<BookmarkIcon fontSize="large"/>}
          />

          <Input
            name="modelo"
            onChange={onChange}
            value={modelo}
            label="Modelo"
            error={errors.modelo}
            required={false}
            placeholder="Modelo"
            icono={<BookmarkIcon fontSize="large"/>}
          />
          <Input
            name="placa"
            onChange={onChange}
            value={placa}
            label="Placa"
            error={errors.placa}
            placeholder="Placa"
            icono={<Looks3Icon fontSize="large"/>}
          />

          <Input
            name="anio"
            onChange={onChange}
            value={anio}
            label="Año"
            error={errors.anio}
            placeholder="Año"
            icono={<CalendarMonthIcon fontSize="large"/>}
          />
      
          <div className="col-12 text-center  mb-6">
            <button className="btn btn-success " type="submit">
              {edit ? "Actualizar " : " Crear "} vehículo
            </button>
          </div>
        </form>
      </>
    </Layout>
  );
};

export default Formulario;
