import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getClienteDropdownAction } from "../../actions/clienteAction";
import { Confirm } from "notiflix/build/notiflix-confirm-aio";
import Layout from "../../Components/Sidebar/Layout";
import {
  erroresApiMostrar,
  validarFormulario,
} from "../../helper/validarFormulario";
import { ContenedorError } from "../../Components/Inputs/ContenedorError";
import SelectSearch from "../../Components/Select/SelectSearch";
import { ItemContenedor } from "../../Components/Contenedores/ItemContenedor";
import RoomIcon from "@mui/icons-material/Room";
import NavigationIcon from '@mui/icons-material/Navigation';
import Agriculture from "@mui/icons-material/Agriculture";
import { Enviar } from "../../Components/button/Enviar";
import { Table } from "../../Components/Table/Table";

const Formulario = () => {
  // Dispara una acción en el action
  const dispatch = useDispatch();

  const [error, setError] = useState(false);

  // Validar
  const [errors, setErrors] = useState({
    marca: "",
    modelo: "",
    placa: "",
    anio: "",
    clienteid: "",
  });

  const [seleccionarCliente, setSeleccionarCliente] = useState("");

  const [vehiculo, setVehiculo] = useState({
    marca: "",
    modelo: "",
    placa: "",
    anio: "",
    clienteid: "",
  });

  const onChange = (e) => {
    setVehiculo({
      ...vehiculo,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    if (seleccionarCliente !== "") {
      setVehiculo({
        ...vehiculo,
        clienteid: seleccionarCliente.value.toString(),
      });
    }

    // eslint-disable-next-line
  }, [seleccionarCliente]);

  // Estado que en el reducer pasa a true si fue enviado
  const sending = useSelector((state) => state.vehiculos.sending);
  const errorEnvio = useSelector((state) => state.vehiculos.error);
  const erroresEnvio = useSelector((state) => state.vehiculos.errores);
  const clientes = useSelector((state) => state.clientes.clientes);

  // Obtener datos para cargar el dropdown
  useEffect(() => {
    dispatch(getClienteDropdownAction());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (sending) {
      clearForm();
    }

    // eslint-disable-next-line
  }, [sending]);

  useEffect(() => {
    if (errorEnvio) {
      setErrors(erroresApiMostrar(vehiculo, erroresEnvio).objErrores);
      setError(true);
    }
    // eslint-disable-next-line
  }, [errorEnvio]);

  const addVehiculo = async (e) => {
    e.preventDefault();
    await setError(false);
    setErrors(validarFormulario(vehiculo).objErrores);

    if (validarFormulario(vehiculo).validarError) {
      setError(true);
    } else {
      alert("Vamos a crear");
    }
  };

  const eliminarMaquinaria = (codigo,maquinaria) => {
    Confirm.show(
      "¿Estás seguro?",
      `La máquina ${maquinaria} se eliminará.`,
      "Si",
      "Cancelar",
      () => {
       console.log(codigo)
      }
    );
  }

  const clearForm = () => {
    setVehiculo({
      marca: "",
      modelo: "",
      placa: "",
      anio: "",
      clienteid: "",
    });
    setSeleccionarCliente("");
  };

  const { marca, modelo, placa, anio } = vehiculo;

  return (
    <Layout titulo="REGISTRO DE MAQUINARIA">
      <>
        {/* Contenedor de Error */}
        <ContenedorError error={error} />

        <form className="row g-3 needs-validation" onSubmit={addVehiculo}>
          
          {/* <ItemContenedor
            titulo="SITIO"
            componente={<RoomIcon fontSize="large" />}
          /> */}
          <SelectSearch
            setChange={setSeleccionarCliente}
            valor={seleccionarCliente}
            opciones={clientes}
            name="Cliente"
            error={errors.clienteid}
            icono={<RoomIcon fontSize="large" />}
          />

          {/* <ItemContenedor
            titulo="UBICACIÓN"
            componente={<NavigationIcon fontSize="large" />}
          /> */}
          <SelectSearch
            setChange={setSeleccionarCliente}
            valor={seleccionarCliente}
            opciones={clientes}
            name="Cliente"
            error={errors.clienteid}
            icono={<RoomIcon fontSize="large" />}
          />

          {/* <ItemContenedor
            titulo="MAQUINARIA"
            componente={<Agriculture fontSize="large" />}
          /> */}
          <SelectSearch
            setChange={setSeleccionarCliente}
            valor={seleccionarCliente}
            opciones={clientes}
            name="Cliente"
            error={errors.clienteid}
            icono={<RoomIcon fontSize="large" />}
          />
          <Table eliminar={eliminarMaquinaria}/>

          <div className={`row col-md-10 col-lg-4 justify-content-between mt-4`} >
              <div className="col-2">ICONO</div>
              <div className="col-10"><h2 className="text">11 Columnas</h2></div>
          </div>

         <Enviar title="ENVIAR FORMULARIO"/>
        </form>
      </>
    </Layout>
  );
};

export default Formulario;
