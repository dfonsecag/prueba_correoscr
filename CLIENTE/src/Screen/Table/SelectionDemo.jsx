import React, { useState } from 'react';

import { kaReducer, Table } from 'ka-table';
import {deselectRow, selectRow, selectRowsRange,
} from 'ka-table/actionCreators';
import {  SortingMode } from 'ka-table/enums';
import { kaPropsUtils } from 'ka-table/utils';
import { search } from 'ka-table/actionCreators';
import { bootstrapChildComponents, SelectionCell, SelectionHeader } from '../../Components/variables/StyleTable';

const dataArray = Array(12).fill(undefined).map(
  (_, index) => ({
    column1: `row:${index}`,
    column2: `row:${index}`,
    column3: `row:${index}`,
    column4: `row:${index}`,
    id: index,
  }),
);


const tablePropsInit = {
  columns: [
    {
      key: 'selection-cell', style: { width: '10%' }
    },
    { key: 'column1', title: 'Nombre', style: { width: '30%' } },
    { key: 'column2', title: 'Direccion', style: { width: '25%' } },
    { key: 'column3', title: 'Descriopcion', style: { width: '25%' } },
    { key: 'column4', title: 'Otros', style: { width: '10%' } },
  ],
  paging: {
    enabled: true,
  },
  data: dataArray,
  rowKeyField: 'id',
  sortingMode: SortingMode.Single,
};

const SelectionDemo = () => {
  const [tableProps, changeTableProps] = useState(tablePropsInit);
  const [checked, setChecked]= useState([]);
  const dispatch = (action) => {
    changeTableProps((prevState) => kaReducer(prevState, action));
  };



  
 
  return (
    <div className={`row col-sm-8 col-md-8 col-lg-8 offset-sm-2 offset-md-2 offset-lg-2  mt-4 `} >
    <input type='search' defaultValue={tableProps.searchText} placeholder="Buscar" onChange={(event) => {
      dispatch(search(event.currentTarget.value));
    }} className='top-element'/>
      <Table
        {...tableProps}
        childComponents={{
          ...bootstrapChildComponents,
          cellText: {
            content: (props) => {
              if (props.column.key === 'selection-cell'){
                return <SelectionCell {...props} setChecked={setChecked} checked={checked} />
              }
            }
          },
          filterRowCell: {
            content: (props) => {
              if (props.column.key === 'selection-cell'){
                return <></>;
              }
            }
          },
          headCell: {
            content: (props) => {
              if (props.column.key === 'selection-cell'){
                return (
                  <SelectionHeader {...props}
                    areAllRowsSelected={kaPropsUtils.areAllFilteredRowsSelected(tableProps)}
                    setChecked={setChecked}
                    dataArray={dataArray}
                  />
                );
              }
            }
          }
        }}
        dispatch={dispatch}
      />
    </div>
  );
};

export default SelectionDemo;