import React, { useState } from 'react';


import { kaReducer, Table } from 'ka-table';
import { search } from 'ka-table/actionCreators';
import { DataType,PagingPosition,SortingMode } from 'ka-table/enums';

// Data que puede ser el reducer 
const dataArray = [
  { id: 1, name: 'Mike Wazowski', score: 80, passed: true },
  { id: 2, name: 'Billi Bob', score: 55, passed: false },
  { id: 3, name: 'Tom Williams', score: 45, passed: false },
  { id: 4, name: 'Kurt Cobain', score: 75, passed: true },
  { id: 5, name: 'Marshall Bruce', score: 77, passed: true },
  { id: 6, name: 'Sunny Fox', score: 33, passed: false },
];

// Inicializacion de la tabla
const tablePropsInit = {
  columns: [
    { key: 'name', title: 'Name', dataType: DataType.String, style: { width: '60%' } },
    { key: 'score', title: 'Score', dataType: DataType.Number, style: { width: '20%' } },
    { dataType: DataType.Boolean, key: 'passed', title: 'Passed',style: { width: '20%' } },
  ],
  data: dataArray,
  paging: {
    enabled: true,
    pageIndex: 0,
    pageSize: 4,
    position: PagingPosition.Bottom
  },
  search: ({ searchText, rowData, column }) => {
    if (column.key === 'passed'){
      return (searchText === 'false' && !rowData.passed) || (searchText === 'true' && rowData.passed);
    }
  },
  sortingMode: SortingMode.Single,
  rowKeyField: 'id',
};

// Dar estilos con bootstrap
const bootstrapChildComponents = {
  table: {
    elementAttributes: () => ({
      className: 'table table-striped table-hover table-bordered'
    })
  },
  tableHead: {
    elementAttributes: () => ({
      className: 'thead-dark'
    })
  },
  noDataRow: {
    content: () => 'No Data Found'
  },
  pagingIndex: {
    elementAttributes: ({ isActive }) => ({
      className: `page-item ${(isActive ? 'active' : '')}`
    }),
    content: ({ text }) => <div className='page-link'>{text}</div>
  },
  pagingPages: {
    elementAttributes: () => ({
      className: 'pagination'
    }),
  }
};

const SearchDemo = () => {
  const [tableProps, changeTableProps] = useState(tablePropsInit);
  const dispatch = (action) => {
    changeTableProps((prevState) => kaReducer(prevState, action));
  };
  return (
     <div className={`row col-sm-8 col-md-8 col-lg-8 offset-sm-2 offset-md-2 offset-lg-2  mt-4 `} >
      <input type='search' defaultValue={tableProps.searchText} placeholder="Buscar" onChange={(event) => {
        dispatch(search(event.currentTarget.value));
      }} className='top-element'/>
      <Table
        {...tableProps}
        childComponents={bootstrapChildComponents}
        dispatch={dispatch}
      />
      </div>
  );
};

export default SearchDemo;