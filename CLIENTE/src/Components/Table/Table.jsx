import React from "react";
import Button from "../Inputs/Button";

export const Table = ({ data = [], eliminar }) => {
  return (
    <div className="col-md-10 col-sm-12 offset-md-1 mt-4">
      <table className="table">
        <thead className="thead-dark">
          <tr>
            {/*  data={['🗑', 'Código', 'Máquina', 'id']} */}
            <th scope="col">Código</th>
            <th scope="col">Maquinaria</th>
            <th scope="col">Eliminar</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>1024</th>
            <td>BACK HOE (KOMATSU WB687 R)</td>
            <td>
              <Button
                titulo={"Eliminar"}
                handleClick={() =>
                  eliminar("1224", "BACK HOE (KOMATSU WB687 R)")
                }
                icon="Delete"
                type="danger"
              />
            </td>
          </tr>
          <tr>
            <th>1024</th>
            <td>BACK HOE (KOMATSU WB687 R)</td>
            <td>
              <Button
                titulo={"Eliminar"}
                handleClick={() =>
                  eliminar("1224", "BACK HOE (KOMATSU WB687 R)")
                }
                icon="Delete"
                type="danger"
              />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};
