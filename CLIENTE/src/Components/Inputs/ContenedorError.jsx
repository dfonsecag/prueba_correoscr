import React from "react";

export const ContenedorError = ({ offset = 1, error }) => {
  return (
    <>
      {error && (
        <div
          className={`alert alert-danger col-sm-8 col-md-8 col-lg-8 offset-sm-2 offset-md-2 offset-lg-2`}
          role="alert"
        >
          Verifique los datos del formulario
        </div>
      )}
    </>
  );
};
