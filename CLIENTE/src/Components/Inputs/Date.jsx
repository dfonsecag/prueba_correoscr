import React from "react";

export const Date = ({ name, label, onChange, value, offset = 1,error }) => {
  return (
    <div className={`col-md-10 col-lg-4 offset-md-${offset}`}>
      <label htmlFor={name} className="form-label">
        {label}
      </label>
      <input
        type="date"
        className="form-control"
        id={name}
        name={name}
        onChange={onChange}
        value={value}
      />
      {error.length > 0 && <div className="feedback"> ** {label} {error}</div>}
    </div>
  );
};
