
import React from "react";

export const Input = ({name,label,onChange,value,icono, error,placeholder, required=true}) => {
  return (
    <div className={`row col-sm-8 col-md-8 col-lg-8 offset-sm-2 offset-md-2 offset-lg-2  mt-4 `} >
     <div className="col-sm-2 col-md-2">{icono}</div>
     <div className="col-sm-10 col-md-10">
      <input
        type="text"
        className="form-control"
        id={name}
        name={name}
        onChange={onChange}
        value={value}
        placeholder={placeholder}
      />
      {
        (error.length > 0 && required) &&
        (
          <div className="feedback"> ** {label} {error}</div>
        )
      }
      </div>
    </div>
  );
};
