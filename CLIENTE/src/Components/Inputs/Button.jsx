import React from "react";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { IconButton } from "@mui/material";

const Button = ({ handleClick, icon }) => {
  return (
    <IconButton
    aria-label="account of current user"
    aria-controls="menu-appbar"
    aria-haspopup="true"
    onClick={handleClick}
    color="inherit"
  >
    {
      icon === 'Edit' ?
      ( <EditIcon />) :
      ( <DeleteIcon /> )
    }
   
  </IconButton>
  );
};

export default Button;
