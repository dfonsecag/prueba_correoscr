import React from "react";

export const Email = ({ name, label, onChange, value, offset=1 , error }) => {
  // console.log(error)
  return (
    <>
      <div className={`col-md-10 col-lg-4 offset-md-${offset}`}>
        <label htmlFor="validationCustomUsername" className="form-label">
          {label}
        </label>
        <div className="input-group has-validation">
          <span className="input-group-text" id="inputGroupPrepend">
            @
          </span>
          <input
            type="email"
            className="form-control"
            id={name}
            name={name}
            onChange={onChange}
            value={value}
          />
        </div>
        {
        error.length > 0 &&
        (
          <div className="feedback"> ** {label} {error}</div>
        )
        }
      </div>
    </>
  );
};
