import React from "react";

export const Enviar = ({ title}) => {

  return (
    <div className="col-12 text-center" style={{marginTop:'90px'}}>
    <button className="btn btn-success btn-lg " type="submit">
     {title}
    </button>
  </div>
  );
};
