import React, { useState } from "react";
import { FaBars } from "react-icons/fa";
import Aside from "./Aside";

function Layout(props) {
  // eslint-disable-next-line
  const [rtl, setRtl] = useState(false);
  // Hacer el side bar pequeño
  // eslint-disable-next-line
  const [collapsed, setCollapsed] = useState(
    window.screen.width > 1500 ? false : true
  );
  // eslint-disable-next-line
  const [image, setImage] = useState(true);
  const [toggled, setToggled] = useState(false);

  const handleToggleSidebar = (value) => {
    setToggled(value);
  };

  return (
    <div className={`app ${rtl ? "rtl" : ""} ${toggled ? "toggled" : ""}`}>
      <Aside
        image={image}
        collapsed={collapsed}
        rtl={rtl}
        toggled={toggled}
        handleToggleSidebar={handleToggleSidebar}
        setCollapsed={setCollapsed}
      />

      <main>
          <div className="row encabezado">
            <div
              className="btn-toggle"
              onClick={() => handleToggleSidebar(true)}
            >
              <FaBars />
            </div>
            <div className="col mt-1"><h2 className="text-center text-white">{props.titulo}</h2></div>
          </div>

        <div>{props.children}</div>
      </main>
    </div>
  );
}

export default Layout;
