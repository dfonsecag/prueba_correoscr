import {deselectAllFilteredRows, selectAllFilteredRows} from 'ka-table/actionCreators';
import {deselectRow, selectRow, selectRowsRange,
} from 'ka-table/actionCreators';

export const bootstrapChildComponents  =  {
    table: {
        elementAttributes: () => ({
          className: 'table table-striped table-hover table-bordered text-center'
        })
      },
      tableHead: {
        elementAttributes: () => ({
          className: 'thead-dark'
        })
      },
      noDataRow: {
        content: () => 'No Data Found'
      },
      pagingIndex: {
        elementAttributes: ({ isActive }) => ({
          className: `page-item ${(isActive ? 'active' : '')}`
        }),
        content: ({ text }) => <div className='page-link'>{text}</div>
      },
      pagingPages: {
        elementAttributes: () => ({
          className: 'pagination'
        }),
      },
      search: ({ searchText, rowData, column }) => {
        if (column.key === 'passed'){
          return (searchText === 'false' && !rowData.passed) || (searchText === 'true' && rowData.passed);
        }
      },

}

// Seleccionar o deseleccionar Todo
export const SelectionHeader = ({
    dispatch, areAllRowsSelected, dataArray, setChecked
  }) => {
    return (
      <input
        type='checkbox'
        checked={areAllRowsSelected}
        onChange={(event) => {
          if (event.currentTarget.checked) {
            setChecked(dataArray)
            dispatch(selectAllFilteredRows()); // also available: selectAllVisibleRows(), selectAllRows()
          } else {
            setChecked([]);
            dispatch(deselectAllFilteredRows()); // also available: deselectAllVisibleRows(), deselectAllRows()
          }
        }}
      />
    );
  };

//   Checked por cada fila
export const SelectionCell = ({
    rowKeyValue, dispatch, isSelectedRow, rowData,selectedRows,setChecked,checked
  }) => {
    return (
      <input
        type='checkbox'
        checked={isSelectedRow}
        onChange={(event) => {
          if (event.nativeEvent.shiftKey){
            dispatch(selectRowsRange(rowKeyValue, [...selectedRows].pop()));           
          } else if (event.currentTarget.checked) {
            dispatch(selectRow(rowKeyValue));
            setChecked([...checked, rowData])
          } else {
            dispatch(deselectRow(rowKeyValue));
            let filtro = checked.filter((p) => p.id !== rowKeyValue);
            setChecked(filtro)
          }
        }}
      />
    );
  };