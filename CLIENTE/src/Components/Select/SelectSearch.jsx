import React from "react";
import CreatableSelect from "react-select/creatable";

const SelectSearch = ({  setChange, valor, opciones, disable = false,icono, titulo = "Seleccionar", name, error ,required=true }) => {
  return (
    <div className={`row col-sm-8 col-md-8 col-lg-8 offset-sm-2 offset-md-2 offset-lg-2  mt-4 `} >
              <div className="col-sm-2 col-md-2">{icono}</div>
              <div className="col-sm-10 col-md-10">
              <CreatableSelect
                className={"basic-single w-"}
                classNamePrefix="select"
                isClearable
                options={opciones}
                placeholder={titulo}
                value={valor}
                isDisabled={disable}
                onChange={(item) => setChange(item)}
              />
              {
                  (error.length > 0 && required) &&
                  (
                    <div className="feedback"> ** {name} {error}</div>
                  )
              }
              </div>
        </div>
          
    // <div className={`col-md-10 col-lg-4 offset-md-${offset}`} >
    // <label htmlFor={name} className="form-label">
    //   {name}
    // </label>
   
    // </div>
  );
};

export default SelectSearch;