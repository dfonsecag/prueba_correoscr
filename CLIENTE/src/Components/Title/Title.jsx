import React from "react";

export const Title = ({ title , height=40}) => {

  return (
    <div className="" style={{ height:`${height}px`}}>
      <h1 className="text-center ">{title}</h1>
    </div>
  );
};
