import React from "react";

const SpinnerCircular = ({texto}) => {
  return (
    <div className="spinner">
      <span
        className="spinner-border spinner-border-md"
        style={{ width: "3rem", height: "3rem" }}
        role="status"
        aria-hidden="true"
      ></span>
      <label className="m-2">{texto}...</label>
    </div>
  );
};

export default SpinnerCircular;
