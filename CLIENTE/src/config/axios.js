import axios from 'axios';

const baseURL = 'http://10.33.0.32:7171/api';

const clienteAxios = axios.create({ baseURL });

export default clienteAxios;