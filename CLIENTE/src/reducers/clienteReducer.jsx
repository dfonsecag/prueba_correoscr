import {
  CLIENTE_ADD,
  CLIENTE_DELETE,
  CLIENTE_ERROR,
  CLIENTE_GET,
  CLIENTE_START,
  CLIENTE_SUCCESS,
  CREATE_CLIENTE_ERROR,
} from "../types";

const initialState = {
  clientes: [],
  clienteEdit: null,
  error: false,
  errores: [],
  loading: false,
  eliminado: false,
  sending: false,
};

// eslint-disable-next-line
export default (state = initialState, action) => {
  switch (action.type) {
    case CLIENTE_START:
      return {
        ...state,
        loading: true,
        error: false,
        sending: false,
        clienteEdit: null,
        eliminado: false,
      };
    case CLIENTE_ERROR:
      return {
        ...state,
        loading: false,
        sending: false,
        clienteEdit: null,
      };
    case CREATE_CLIENTE_ERROR: {
      return {
        ...state,
        error: true,
        sending: false,
        clienteEdit: null,
        errores: action.payload,
      };
    }
    case CLIENTE_SUCCESS:
      return {
        ...state,
        loading: false,
        sending: false,
        error: false,
        clientes: action.payload,
      };

    case CLIENTE_ADD:
      return {
        ...state,
        loading: false,
        error: false,
        clienteEdit: null,
        sending: true,
        errores: [],
      };
    case CLIENTE_DELETE:
      return {
        ...state,
        loading: false,
        error: false,
        eliminado: true,
        clientes: state.clientes.filter((p) => p.id !== action.payload),
      };
    case CLIENTE_GET:
      return {
        ...state,
        loading: false,
        error: false,
        clienteEdit: action.payload,
      };
    default:
      return state;
  }
};
