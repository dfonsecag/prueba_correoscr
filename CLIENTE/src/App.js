import React, { useState } from "react";
/** Redux */
import { Provider } from "react-redux";
import store from "./store";
// Rutas
import Routes from "./Routes";
import { BrowserRouter as Router } from "react-router-dom";

import { IntlProvider } from "react-intl";
import messages from "./messages";
import "./styles/App.scss";

function App() {
  const [locale, setLocale] = useState("en");

  return (
    <IntlProvider locale={locale} messages={messages[locale]}>
      <Router>
        <Provider store={store}>
          <Routes setLocale={setLocale} />
        </Provider>
      </Router>
    </IntlProvider>
  );
}

export default App;
