import clienteAxios from "../config/axios";
import { Report } from 'notiflix/build/notiflix-report-aio';
import { Notify } from 'notiflix/build/notiflix-notify-aio';
import {
  CREATE_VEHICULO_ERROR,
  VEHICULO_ADD,
  VEHICULO_DELETE,
  VEHICULO_ERROR,
  VEHICULO_GET,
  VEHICULO_START,
  VEHICULO_SUCCESS,
} from "../types";

// Obtiene todos los vehiculos
export const getVehiculosAction = () => {
  return async (dispatch) => {
    dispatch(vehiculoStart());
    try {
      const response = await clienteAxios.get(`/vehiculos`);
      // setTimeout(() => {
        dispatch(vehiculoSuccess(response.data));
      // }, 1000);
     
    } catch (error) {
      dispatch(vehiculoError());
    }
  };
};

// Obtiene un vehiculo en especifico por Id
export const getVehiculoIdAction = (id) => {
  return async (dispatch) => {
    dispatch(vehiculoStart());
    try {
      const response = await clienteAxios.get(`/vehiculos/${id}`);
      dispatch(getVehiculoSuccess(response.data[0]));
    } catch (error) {
      dispatch(vehiculoError());
    }
  };
};

// Agregar o editar  un vehiculo
export const addVehiculoAction = (data, edit, id) => {
  return async (dispatch) => {
    dispatch(vehiculoStart());
    try {
      if (!edit) {
        id = null;
        await clienteAxios.post(`/vehiculos`, data);
      } else {
        data.id = id;
        await clienteAxios.put(`/vehiculos/${id}`, data);
      }
      Notify.success(`Se ${edit ? "actualizó" : "creó"} exitosamente el vehículo !`);
      dispatch(addVehiculoSuccess());
    } catch (error) {
      Notify.failure(`No se ${edit ? "actualizó" : "creó"} el vehículo !`);
      dispatch(crearVehiculoError(error.response.data.errors));
    }
  };
};

// Eliminar un vehiculo
export const deleteVehiculoAction = (id) => {
  return async (dispatch) => {
    dispatch(vehiculoStart());
    try {
      await clienteAxios.delete(`/vehiculos/${id}`);
      Notify.success(`Se eliminó exitosamente el vehículo !`);
      dispatch(deleteVehiculoSuccess(id));
    } catch (error) {
      dispatch(vehiculoError());     
      Report.failure(
        'Hubo un error!',
        'Intente nuevamente',
        'Ok',
        );
    }
  };
};

export const vehiculoStart = () => ({
  type: VEHICULO_START,
});

export const vehiculoSuccess = (data) => ({
  type: VEHICULO_SUCCESS,
  payload: data,
});

export const vehiculoError = () => ({
  type: VEHICULO_ERROR,
});

export const crearVehiculoError = (data) => ({
  type: CREATE_VEHICULO_ERROR,
  payload: data,
});

export const addVehiculoSuccess = () => ({
  type: VEHICULO_ADD,
});

export const deleteVehiculoSuccess = (id) => ({
  type: VEHICULO_DELETE,
  payload: id,
});

export const getVehiculoSuccess = (data) => ({
  type: VEHICULO_GET,
  payload: data,
});
