import clienteAxios from "../config/axios";
import { Report } from 'notiflix/build/notiflix-report-aio';
import { Notify } from 'notiflix/build/notiflix-notify-aio';

import { CLIENTE_ADD, CLIENTE_CLEAR, CLIENTE_DELETE, CLIENTE_ERROR, CLIENTE_GET, CLIENTE_START, CLIENTE_SUCCESS, CREATE_CLIENTE_ERROR } from "../types";

// Obtiene todos los clientes
export const getClienteAction = () => {
  return async (dispatch) => {
    dispatch(clienteStart());
    try {
      const response = await clienteAxios.get(`/clientes`);
      // setTimeout(() => {
        dispatch(clienteSuccess(response.data));
      // }, 1000);
     
    } catch (error) {
      dispatch(clienteError());
    }
  };
};

// Obtiene todos los clientes
export const getClienteDropdownAction = () => {
  return async (dispatch) => {
    dispatch(clienteStart());
    try {
      const response = await clienteAxios.get(`/clientes/dropdown`);
      dispatch(clienteSuccess(response.data));
    } catch (error) {
      dispatch(clienteError());
    }
  };
};

// Obtiene un cliente en especifico por Id
export const getClienteIdAction = (id) => {
  return async (dispatch) => {
    dispatch(clienteStart());
    try {
      const response = await clienteAxios.get(`/clientes/${id}`);
      
      dispatch(getClienteSuccess(response.data));
    } catch (error) {
      dispatch(clienteError());
    }
  };
};

// Agregar o editar  un cliente
export const addClienteAction = (data,edit, id) => {
  return async (dispatch) => {
    dispatch(clienteStart());
    try {
      if (!edit) {
        await clienteAxios.post(`/clientes`, data);
      } else {
        await clienteAxios.put(`/clientes/${id}`, data);
      }
      Notify.success(`Se ${edit ? "actualizó" : "creó"} exitosamente el cliente !`);
      dispatch(getClienteAction())
      dispatch(addClienteSuccess());
    } catch (error) {
      Notify.failure(`No se ${edit ? "actualizó" : "creó"} el cliente !`);
      dispatch(crearClienteError(error.response.data.errors));
    }
  };
};

// Eliminar un cliente
export const deleteClienteAction = (id) => {
  return async (dispatch) => {
    dispatch(clienteStart());
    try {
      await clienteAxios.delete(`/clientes/${id}`);
      Notify.success(`Se eliminó exitosamente el cliente !`);
      dispatch(deleteClienteSuccess(id));    
      
    } catch (error) {
       Report.failure(
        'Hubo un error!',
        'Existe un vehículo registrado a este cliente',
        'Ok',
        );
      dispatch(clienteError());
      
    }
  };
};



export const clienteStart = () => ({
  type: CLIENTE_START,
});

export const clienteSuccess = (data) => ({
  type: CLIENTE_SUCCESS,
  payload: data,
});

export const clienteError = () => ({
  type: CLIENTE_ERROR,
});

export const crearClienteError = (data) => ({
  type: CREATE_CLIENTE_ERROR,
  payload:data
});

export const addClienteSuccess = () => ({
  type: CLIENTE_ADD,
});

export const deleteClienteSuccess = (id) => ({
  type: CLIENTE_DELETE,
  payload: id,
});

export const getClienteSuccess = (data) => ({
  type: CLIENTE_GET,
  payload: data,
});

export const clearOwnerAction = () => ({
  type: CLIENTE_CLEAR,
});