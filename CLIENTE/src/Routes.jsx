import React from "react";
import Home from "./Screen/Home/Home";
import Prueba from "./Screen/Home/Prueba";
import { Switch, Route } from "react-router-dom";
import IndexClient from "./Screen/Client/Index";
import FormularioCliente from "./Screen/Client/Formulario";
import FormularioVehiculo from "./Screen/Vehiculo/Formulario";
import FormularioMaquinaria from "./Screen/Maquinaria/Formulario";
import IndexVehiculo from "./Screen/Vehiculo/Index";
import SearchDemo from "./Screen/Table/SearchDemo";
import SelectionDemo from "./Screen/Table/SelectionDemo";

const Routes = () => {
  return (
    <>
      <Switch>
        {/* Inicio */}
        <Route exact path={"/"} component={Prueba}/>
        {/* Rutas de Cliente */}
        <Route exact path={"/clients/create"} component={FormularioCliente}/>
        <Route exact path={"/clients/edit/:id"} component={FormularioCliente}/>
        <Route exact path={"/clients"} component={IndexClient}/>

         {/* Rutas de Vehiculo */}
        <Route exact path={"/vehiculos/crear"} component={FormularioVehiculo}/>
        <Route exact path={"/vehiculos/edit/:id"} component={FormularioVehiculo}/>
        <Route exact path={"/vehiculos"} component={IndexVehiculo}/>

        {/* Rutas de Maquinaria */}
        <Route exact path={"/maquinaria/crear"} component={FormularioMaquinaria}/>

        {/* Rutas de Tablas */}
        <Route exact path={"/search"} component={SearchDemo}/>
        <Route exact path={"/selection"} component={SelectionDemo}/>
        SelectionDemo

       
      </Switch>
    </>
  );
};

export default Routes;
