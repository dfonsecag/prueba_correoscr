import AgricultureIcon from "@mui/icons-material/Agriculture";
import AssignmentIcon from '@mui/icons-material/Assignment';

export const MenuAdmin = [
  {
    titulo: "Clientes",
    ruta: "/clients",
    componente: <AgricultureIcon fontSize="large" />
  },
  {
    titulo: "Vehículos",
    ruta: "/vehiculos",
    componente: <AssignmentIcon fontSize="large" />
  },
  {
    titulo: "Maquinaria",
    ruta: "/maquinaria/crear",
    componente: <AgricultureIcon fontSize="large" />
  },
  {
    titulo: "Bitácora",
    ruta: "/vehiculos",
    componente: <AssignmentIcon fontSize="large" />
  },
];
