import { combineReducers } from "redux";
import clienteReducer from "./clienteReducer";
import vehiculoReducer from "./vehiculoReducer";

export default combineReducers({
  clientes:clienteReducer,
  vehiculos:vehiculoReducer
});
