import { CREATE_VEHICULO_ERROR, VEHICULO_ADD, VEHICULO_DELETE, VEHICULO_ERROR, VEHICULO_GET, VEHICULO_START, VEHICULO_SUCCESS } from "../types";


const initialState = {
  vehiculos: [],
  vehiculoEdit: null,
  error: false,
  errores:[],
  loading: false,
  eliminado:false,
  sending: false,
};

// eslint-disable-next-line
export default (state = initialState, action) => {
  switch (action.type) {
    case VEHICULO_START:
      return {
        ...state,
        loading: true,
        error: false,
        sending: false,
        vehiculoEdit: null,
        eliminado:false
      };
    case VEHICULO_ERROR:
      return {
        ...state,
        loading: false,
        sending: false,
        vehiculoEdit: null,
      };
    case CREATE_VEHICULO_ERROR:
      {
        return {
          ...state,
          error: true,
          sending: false,
          vehiculoEdit: null,
          errores:action.payload
        }
      }
    case VEHICULO_SUCCESS:
      return {
        ...state,
        loading: false,
        sending: false,
        error: false,
        vehiculos: action.payload,
      };
  
    case VEHICULO_ADD:
      return {
        ...state,
        loading: false,
        error: false,
        vehiculoEdit: null,
        sending: true,
        errores:[]
      };
    case VEHICULO_DELETE:
      return {
        ...state,
        loading: false,
        error: false,
        eliminado:true,
        vehiculos: state.vehiculos.filter(
          (p) => p.id !== action.payload
        ),
      };
    case VEHICULO_GET:
      return {
        ...state,
        loading: false,
        error: false,
        vehiculoEdit: action.payload,
      };
    default:
      return state;
  }
};
