import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addClienteAction,
  getClienteIdAction,
} from "../../actions/clienteAction";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import { Email } from "../../Components/Inputs/Email";
import { Input } from "../../Components/Inputs/Input";
import Layout from "../../Components/Sidebar/Layout";
import { Date } from "../../Components/Inputs/Date";
import { erroresApiMostrar, validarFormulario } from "../../helper/validarFormulario";
import { ContenedorError } from "../../Components/Inputs/ContenedorError";

const Formulario = () => {
  // Parámetro del id
  const { id } = useParams();

  // Historial del router
  const history = useHistory();

  // Dispara una acción en el action
  const dispatch = useDispatch();

  // estado para si es un formulario para editar o crear
  const [edit, setEdit] = useState(false);
  const [error, setError] = useState(false);

  // Validar
  const [errors, setErrors] = useState({
    nombre: "",
    primerApellido: "",
    segundoApellido: "",
    cedula: "",
    fechaNacimiento: "",
    correo: "",
    telefono: "",
  });

  const [cliente, setCliente] = useState({
    nombre: "",
    primerApellido: "",
    segundoApellido: "",
    cedula: "",
    fechaNacimiento: "",
    correo: "",
    telefono: "",
  });

  const onChange = (e) => {
    setCliente({
      ...cliente,
      [e.target.name]: e.target.value,
    });
  };

  // Estado que en el reducer pasa a true si fue enviado
  const sending = useSelector((state) => state.clientes.sending);

  const errorEnvio = useSelector((state) => state.clientes.error);
  const erroresEnvio = useSelector((state) => state.clientes.errores);

  // Obtiene los datos si se esta editando un cliente
  const clienteEdit = useSelector((state) => state.clientes.clienteEdit);

  useEffect(() => {
    if (id !== undefined) {
      setEdit(true);
      dispatch(getClienteIdAction(id));
    } else {
      setEdit(false);

      clearForm();
    }
    // eslint-disable-next-line
  }, [id]);

  useEffect(() => {
    if (clienteEdit !== null) {
      clienteEdit.fechaNacimiento = clienteEdit.fechaNacimiento.substring(
        0,
        10
      );
      setCliente(clienteEdit);
    }
    // eslint-disable-next-line
  }, [clienteEdit]);

  useEffect(() => {
    if (sending) {
      clearForm();
      
      if(sending && edit){
        history.push(`/clients`);
      }
    }     

    // eslint-disable-next-line
  }, [sending]);

  useEffect(() => {
    if (errorEnvio) {
      setErrors( erroresApiMostrar(cliente, erroresEnvio).objErrores);
      setError(true);    
    }
    // eslint-disable-next-line
  }, [errorEnvio]);

  
  const addClient = async(e) => {
    e.preventDefault();
    await setError(false);
    setErrors(validarFormulario(cliente).objErrores);  

    if (validarFormulario(cliente).validarError) {
      setError(true);
    } else {
      setError(false);
      dispatch(addClienteAction(cliente, edit, id));
    }
  };

  const clearForm = () => {
    setCliente({
      nombre: "",
      primerApellido: "",
      segundoApellido: "",
      cedula: "",
      fechaNacimiento: "",
      correo: "",
      telefono: "",
    });
  };

  const {
    nombre,
    primerApellido,
    segundoApellido,
    cedula,
    fechaNacimiento,
    correo,
    telefono,
  } = cliente;

  return (
    <Layout titulo={edit ? "Editar Cliente" : "Crear Cliente"}>
      <>
        {/* Contenedor de Error */}
        <ContenedorError error={error} />

        <form className="row g-3 needs-validation" onSubmit={addClient}>
          {/* Input de Nombre */}
          <Input
            name="nombre"
            onChange={onChange}
            value={nombre}
            label="Nombre"
            error={errors.nombre}
          />

          <Input
            name="primerApellido"
            onChange={onChange}
            value={primerApellido}
            label="Primer Apellido"
            error={errors.primerApellido}
          />

          <Input
            name="segundoApellido"
            onChange={onChange}
            value={segundoApellido}
            label="Segundo Apellido"
            error={errors.segundoApellido}
          />

          <Email
            name="correo"
            type="email"
            onChange={onChange}
            value={correo}
            label="Correo Electrónico"
            error={errors.correo}
          />

          <Input
            name="cedula"
            onChange={onChange}
            value={cedula}
            label="Cédula"
            error={errors.cedula}
          />

          <Date
            name="fechaNacimiento"
            onChange={onChange}
            value={fechaNacimiento}
            label="Fecha Nacimiento"
            error={errors.fechaNacimiento}
          />

          <Input
            name="telefono"
            onChange={onChange}
            value={telefono}
            label="Teléfono"
            error={errors.telefono}
          />

          <div className="col-12 text-center  mb-6">
            <button className="btn btn-success " type="submit">
              {edit ? "Actualizar " : " Crear "} cliente
            </button>
          </div>
        </form>
      </>
    </Layout>
  );
};

export default Formulario;
