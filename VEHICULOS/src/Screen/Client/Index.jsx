import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { PropertyData } from "../../Components/variables";
import Datatable from "../../Components/datatable/Datatable";
import { Loading } from 'notiflix/build/notiflix-loading-aio';
import { Confirm } from "notiflix/build/notiflix-confirm-aio";
import { useHistory } from "react-router-dom";
import {
  getClienteAction,
  deleteClienteAction,
} from "../../actions/clienteAction";
import Layout from "../../Components/Sidebar/Layout";

const Cliente = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const clientesRequest = useSelector((state) => state.clientes.clientes);

  const loading = useSelector((state) => state.clientes.loading);

  let height =  window.screen.height / 20 ;
  console.log('Pantalla',height*20)

  console.log('Div de tabla',height*13)

  useEffect(() => {
    if(loading)
    Loading.dots('Cargando ...');
    else
    Loading.remove();
    // eslint-disable-next-line
  }, [loading]);

  useEffect(() => {
    dispatch(getClienteAction());
    // eslint-disable-next-line
  }, []);

  /**Eliminar propiedad */
  const deleteCliente = (id, nombre) => {
    Confirm.show(
      "¿Estás seguro?",
      `El cliente ${nombre} se eliminará.`,
      "Si",
      "Cancelar",
      () => {
        dispatch(deleteClienteAction(id));
      }
    );
  };

  /**Obtener informacion de la fila a editar */
  const getClienteId = (row) => {
    history.push(`/clients/edit/${row.id}`);
  };

  const datatable = PropertyData(clientesRequest, deleteCliente, getClienteId);

  return (
    <Layout titulo="Lista Clientes">
      
      <div className="row" style={{ height:`${height*15 }px`}}>
            <div className="datatable-container col-12 mx-auto mt-1" >
              {!loading && (
                <Datatable datos={datatable} />
              )}
            </div>
      </div>
    </Layout>
  );
};

export default Cliente;
