import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { VehiculosData } from "../../Components/variables";
import Datatable from "../../Components/datatable/Datatable";
import { Confirm } from "notiflix/build/notiflix-confirm-aio";
import { Loading } from 'notiflix/build/notiflix-loading-aio';
import { useHistory } from "react-router-dom";
import Layout from "../../Components/Sidebar/Layout";
import {
  getVehiculosAction,
  deleteVehiculoAction,
} from "../../actions/vehiculoAction";

const Cliente = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const vehiculosRequest = useSelector((state) => state.vehiculos.vehiculos);

  const loading = useSelector((state) => state.vehiculos.loading);

 

  useEffect(() => {
    dispatch(getVehiculosAction());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if(loading)
    Loading.dots('Cargando ...');
    else
    Loading.remove();
    // eslint-disable-next-line
  }, [loading]);

  /**Eliminar propiedad */
  const deleteCliente = (id, placa) => {
    Confirm.show(
      "¿Estás seguro?",
      `El vehiculo placa ${placa} se eliminará.`,
      "Si",
      "Cancelar",
      () => {
        dispatch(deleteVehiculoAction(id));
      }
    );
  };

  /**Obtener informacion de la fila a editar */
  const getVehiculoId = (row) => {
    history.push(`/vehiculos/edit/${row.id}`);
  };

  const datatable = VehiculosData(
    vehiculosRequest,
    deleteCliente,
    getVehiculoId
  );

  return (
    <Layout titulo="Lista Vehículos">    

      <div className="row">
            <div className="col-12 mx-auto mt-1">
              {!loading && (
                <Datatable datos={datatable} />
              )}
            </div>
      </div>
    </Layout>
  );
};

export default Cliente;
