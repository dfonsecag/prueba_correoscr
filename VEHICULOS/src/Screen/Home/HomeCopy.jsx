import React from "react";
import Layout from "../../Components/Sidebar/Layout";

const HomeCopy = () => {
  let height = window.screen.height - 300;
  return (
    <Layout titulo="INICIO">
      <div className="div-principal" style={{height:`${height}px`}}>
        <div>
          <h1 className="text-center">¡ Bienvenido !</h1>
          <h2 className="text-center">Demo usando Ionic</h2>
        </div>
      </div>
    </Layout>
  );
};

export default HomeCopy;
