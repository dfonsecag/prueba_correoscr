import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Layout from "../../Components/Sidebar/Layout";
import { MenuAdmin } from "../../data/MenuAdmin";

import { Menu } from "../../Components/Contenedores/Menu";

const Home = () => {
  // Historial del router
  const history = useHistory();
  // State Menu
  // eslint-disable-next-line
  const [menu, setMenu] = useState(MenuAdmin);

  const redirect = (route) => {
    history.push(`${route}`);
  };

  return (
    <Layout titulo="INICIO">
      <div className="row">
        {menu.map((e) => (
          <Menu
            key={e.titulo}
            titulo={e.titulo}
            ruta={e.ruta}
            componente={e.componente}
            redirect={redirect}
          />
        ))}
      </div>
    </Layout>
  );
};

export default Home;
