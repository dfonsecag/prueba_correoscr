export const validarFormulario = (data) => {
  let objErrores = {};
  let validarError = false;
  for (const i in data) {
    // data[i].trim()
    if (data[i] === "") {
      objErrores = { ...objErrores, [[i]]: ` es requerido` };
      validarError = true;
    } else {
      objErrores = { ...objErrores, [[i]]: "" };
    }
  }
  return { objErrores, validarError };
};

export const erroresApiMostrar = (data, errores) => {
  let objErrores = {};
  let validarError = false;
  let msg = '';

  for (const i in data) {
    msg = '';
    // eslint-disable-next-line
    const error = errores.filter( x => x.param == [[i]]);
    if(error.length > 0){
        // eslint-disable-next-line
        error.map(e => {
            if(msg.length > 0)            
            msg += ` - ${e.msg}`
            else
            msg += `${e.msg}`
        });
        objErrores = { ...objErrores, [[i]]: msg };
    }else{
        objErrores = { ...objErrores, [[i]]: "" };
    }
  }
  return { objErrores, validarError };
};
