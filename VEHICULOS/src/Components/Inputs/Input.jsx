
import React from "react";

export const Input = ({name,label,onChange,value,offset=1, error, required=true}) => {
  return (
    <div className={`col-md-10 col-lg-4 offset-md-${offset}`} >
      <label htmlFor={name} className="form-label">
        {label}
      </label>
      <input
        type="text"
        className="form-control"
        id={name}
        name={name}
        onChange={onChange}
        value={value}
      />
      {
        (error.length > 0 && required) &&
        (
          <div className="feedback"> ** {label} {error}</div>
        )
      }
    </div>
  );
};
