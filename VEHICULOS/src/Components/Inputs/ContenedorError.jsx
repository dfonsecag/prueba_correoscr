import React from "react";

export const ContenedorError = ({ offset = 1, error }) => {
  return (
    <>
      {error && (
        <div
          className={`alert alert-danger col-md-10 col-lg-4 offset-md-${offset}`}
          role="alert"
        >
          Verifique los datos del formulario
        </div>
      )}
    </>
  );
};
