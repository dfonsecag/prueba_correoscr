import Button from "../Inputs/Button";

export const PropertyData = (info, deleteCliente, getPropertyType) => {
  const data = {
    columns: [
      {
        label: "Cédula",
        field: "cedula",
        width: 150,
      },
      {
        label: "Nombre Completo",
        field: "nombre",
        width: 150,
      },
      {
        label: "Fecha Nacimiento",
        field: "fechaNacimiento",
        width: 150,
      },
      {
        label: "Correo",
        field: "correo",
        width: 150,
      },
      {
        label: "Teléfono",
        field: "telefono",
        width: 150,
      },
      {
        label: "Acciones",
        field: "accion",
        width: 150,
      },
      
    ],
    rows:
      info.length > 0
        ? info.map((e) => ({
            cedula: e.cedula,
            nombre: e.nombre,
            fechaNacimiento: e.fechanacimiento,
            correo: e.correo,
            telefono: e.telefono,
            accion: (
              <div className="d-flex">
                <Button
                  titulo={"Editar"}
                  handleClick={() => getPropertyType(e)}
                  icon="Edit"
                  type="Success"
                />
                &nbsp;
                <Button
                  titulo={"Eliminar"}
                  handleClick={() => deleteCliente(e.id,e.nombre)}
                  icon="Delete"
                  type="danger"
                />
              </div>
            ),
          }))
        : [],
  };
  return data;
};

export const VehiculosData = (info, deleteCliente, getVehiculoId) => {
  const data = {
    columns: [
      {
        label: "Cliente",
        field: "cliente",
        width: 150,
      },
      {
        label: "Marca",
        field: "marca",
        width: 150,
      },
      {
        label: "Modelo",
        field: "modelo",
        width: 150,
      },
      {
        label: "Placa",
        field: "placa",
        width: 150,
      },
      {
        label: "Año",
        field: "anio",
        width: 150,
      },
      {
        label: "Acciones",
        field: "accion",
        width: 150,
      },
      
    ],
    rows:
      info.length > 0
        ? info.map((e) => ({
            cliente: e.cliente,
            marca: e.marca,
            modelo: e.modelo,
            placa: e.placa,
            anio: e.anio,
            accion: (
              <div className="d-flex">
                <Button
                  titulo={"Editar"}
                  handleClick={() => getVehiculoId(e)}
                  icon="Edit"
                  type="Success"
                />
                &nbsp;
                <Button
                  titulo={"Eliminar"}
                  handleClick={() => deleteCliente(e.id,e.placa)}
                  icon="Delete"
                  type="danger"
                />
              </div>
            ),
          }))
        : [],
  };
  return data;
};
