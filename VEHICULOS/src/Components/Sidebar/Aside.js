import React, { useState } from "react";
import { useIntl } from "react-intl";
import { Link, NavLink } from "react-router-dom";
import { useLocation } from "react-router-dom";
import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from "react-pro-sidebar";
// Iconos
import HomeIcon from "@mui/icons-material/Home";
import PeopleIcon from "@mui/icons-material/People";
import AddIcon from "@mui/icons-material/Add";
import ViewListIcon from "@mui/icons-material/ViewList";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import DirectionsCarIcon from '@mui/icons-material/DirectionsCar';

import sidebarBg from "./../../assets/bg2.jpg";

const Aside = ({
  image,
  collapsed,
  rtl,
  toggled,
  handleToggleSidebar,
  setCollapsed,
}) => {
  const intl = useIntl();
  const location = useLocation();
  // Menu de cliente
  const [client, setClient] = useState(
    location.pathname.split("/")[1] === "clients" ? true : false
  );
  // Menu de vehiculos
  const [vehiculo, setVehiculo] = useState(
    location.pathname.split("/")[1] === "vehiculos" ? true : false
  );

  return (
    <ProSidebar
      image={image ? sidebarBg : false}
      rtl={rtl}
      collapsed={collapsed}
      toggled={toggled}
      breakPoint="md"
      onToggle={handleToggleSidebar}
    >
      <SidebarHeader>
        <div
          style={{
            padding: "24px",
            textTransform: "uppercase",
            fontWeight: "bold",
            fontSize: 14,
            letterSpacing: "1px",
            overflow: "hidden",
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
          }}
        >
          Demo
        </div>
      </SidebarHeader>

      <SidebarContent>
        <Menu iconShape="circle">
          <MenuItem
            icon={<HomeIcon />}
            suffix={
              <span className="badge red">
                {intl.formatMessage({ id: "new" })}
              </span>
            }
          >
            <NavLink exact to={"/"}>
              Inicio
            </NavLink>
          </MenuItem>
        </Menu>
        {/* Clientes */}
        <Menu iconShape="circle">
          <SubMenu
            suffix={<span className="badge yellow">2</span>}
            title="Módulo Clientes"
            icon={<PeopleIcon />}
            data-element={location.pathname}
            open={client}
            onOpenChange={() => setClient(!client)}
          >
            <MenuItem icon={<AddIcon />}>
              Crear Cliente
              <Link to="/clients/create" />
            </MenuItem>
            <MenuItem icon={<ViewListIcon />}>
              Clientes
              <Link to="/clients" />
            </MenuItem>
          </SubMenu>
        </Menu>

         {/* Vehiculos */}
         <Menu iconShape="circle">
          <SubMenu
            suffix={<span className="badge yellow">2</span>}
            title="Módulo Vehículos"
            icon={<DirectionsCarIcon />}
            data-element={location.pathname}
            open={vehiculo}
            onOpenChange={() => setVehiculo(!vehiculo)}
          >
            <MenuItem icon={<AddIcon />}>
              Crear Vehiculo
              <Link to="/vehiculos/crear" />
            </MenuItem>
            <MenuItem icon={<ViewListIcon />}>
              Vehículos
              <Link to="/vehiculos" />
            </MenuItem>
          </SubMenu>
        </Menu>
        {/* Otro Menu */}
       
      </SidebarContent>

      <SidebarFooter style={{ textAlign: "center" }}>
        <div
          className="sidebar-btn-wrapper"
          style={{
            padding: "20px 24px",
          }}
          onClick={() => setCollapsed(!collapsed)}
        >
          {!collapsed ? <ArrowBackIosNewIcon /> : <ArrowForwardIosIcon />}
        </div>
      </SidebarFooter>
    </ProSidebar>
  );
};

export default Aside;
