import React from "react";
import CreatableSelect from "react-select/creatable";

const SelectSearch = ({  setChange, valor, opciones, disable = false, titulo = "Seleccionar", name,offset=1, error ,required=true }) => {
  return (
    <div className={`col-md-10 col-lg-4 offset-md-${offset}`} >
    <label htmlFor={name} className="form-label">
      {name}
    </label>
    <CreatableSelect
      className={"basic-single w-"}
      classNamePrefix="select"
      isClearable
      options={opciones}
      placeholder={titulo}
      value={valor}
      isDisabled={disable}
      onChange={(item) => setChange(item)}
    />
     {
        (error.length > 0 && required) &&
        (
          <div className="feedback"> ** {name} {error}</div>
        )
      }
    </div>
  );
};

export default SelectSearch;