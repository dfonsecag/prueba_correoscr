import React from "react";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";

export const Menu = ({ titulo, ruta, componente, redirect }) => {
  let height = window.screen.height / 12;
  return (
    <>
      <div className="row col-lg-4 col-md-6 col-sm-12 justify-content-between mt-4" style={{height:`${height}px`, cursor:'pointer'}} onClick={()=>redirect(ruta)}>
        <div className="col-1"> {componente}</div>
        <div className="col-7 offset-md-2"><h2 className="text">{titulo}</h2></div>
        <div className="col-2"> <ArrowForwardIosIcon fontSize="large" /></div>
        <div className="dropdown-divider"></div>
    </div>
    </>
  );
};
