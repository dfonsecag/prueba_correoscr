import React from "react";

export const ItemContenedor = ({ titulo, componente, offset = 1 }) => {
  return (
    <div className={`row col-md-10 col-lg-4 justify-content-between mt-4 offset-md-${offset}`} >
        <div className="col-1"> {componente}</div>
        <div className="col-11"><h2 className="text">{titulo}</h2></div>
    </div>
  );
};
