import React from "react";
import Home from "./Screen/Home/Home";
import { Switch, Route } from "react-router-dom";
import IndexClient from "./Screen/Client/Index";
import FormularioCliente from "./Screen/Client/Formulario";
import FormularioVehiculo from "./Screen/Vehiculo/Formulario";
import FormularioMaquinaria from "./Screen/Maquinaria/Formulario";
import IndexVehiculo from "./Screen/Vehiculo/Index";

const Routes = () => {
  return (
    <>
      <Switch>
        {/* Inicio */}
        <Route exact path={"/"} component={Home}/>
        {/* Rutas de Cliente */}
        <Route exact path={"/clients/create"} component={FormularioCliente}/>
        <Route exact path={"/clients/edit/:id"} component={FormularioCliente}/>
        <Route exact path={"/clients"} component={IndexClient}/>

         {/* Rutas de Vehiculo */}
        <Route exact path={"/vehiculos/crear"} component={FormularioVehiculo}/>
        <Route exact path={"/vehiculos/edit/:id"} component={FormularioVehiculo}/>
        <Route exact path={"/vehiculos"} component={IndexVehiculo}/>

        {/* Rutas de Maquinaria */}
        <Route exact path={"/maquinaria/crear"} component={FormularioMaquinaria}/>
       
      </Switch>
    </>
  );
};

export default Routes;
